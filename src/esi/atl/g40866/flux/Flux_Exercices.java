package esi.atl.g40866.flux;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author G40866
 */
public class Flux_Exercices {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.err.println("Il n'y a pas de chemin.");
            System.exit(0);
        }
        String cheminAcces = args[0];
        File f = new File(cheminAcces);
        if (!f.exists()) {
            System.err.println("Le fichier ou le chemin n'existe pas.");
            System.exit(0);
        }

        System.out.println("Le chemin absolu du fichier: " + f.getName() + "\n"
                + f.getAbsolutePath() + "\n");
        System.out.println("Le dossier parent du fichier: " + f.getName() + "\n"
                + f.getParent() + "\n");
        System.out.println("Le fichier " + f.getName() + "a été modifié " 
                + f.lastModified() + "\n");

        if (f.isFile()) {
            System.out.println(f.getName() + " est un fichier." + "\n");
            System.out.println("La taille du fichier: " + f.getName() +
                " en bytes: " + f.getTotalSpace() + "\n");
        }

    }

}
